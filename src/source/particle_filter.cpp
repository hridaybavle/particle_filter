#include "particle_filter.h"

particle_filter::particle_filter()
{
    std::cout << "particle filter constructor " << std::endl;
    iteration_num_ = 0;
}

particle_filter::~particle_filter()
{
    std::cout << "particle filter destructor " << std::endl;
}


inline bool comparator(const particle_filter::obs_vector& lhs, const particle_filter::obs_vector& rhs)
{
    return lhs.maha_dist < rhs.maha_dist;
}

inline bool highToLow(const float& lhs, const float& rhs)
{
    return lhs > rhs;
}

inline bool particlesHighToLow(const particle_filter::particles& lhs, particle_filter::particles& rhs)
{
    return lhs.weight > rhs.weight;
}

inline bool comparatorlowToHigh(const float& lhs, const float& rhs)
{
    return lhs < rhs;
}

std::vector<Eigen::VectorXf> particle_filter::init(int state_size, int num_particles, std::vector<Eigen::Vector3f> aruco_poses, std::vector<object_info_struct_pf> mapped_objects)
{
    this->state_size_ = state_size;
    this->num_particles_ = num_particles;

    std::default_random_engine gen;

    // --- This is for testing the discrete distribution --//

    //    const int nrolls = 10;    // number of experiments
    //    const int nstars = 10;   // maximum number of stars to distribute
    //    std::default_random_engine generator;
    //    std::discrete_distribution<int> distribution {1,2,1,1,2,2,1,1,2,20};

    //    int p[10]={};

    //    for (int i=0; i<nrolls; ++i) {
    //        int number = distribution(generator);
    //        std::cout  << "distribution max " <<   distribution.max() << std::endl;
    //        std::cout << "generator " << generator << std::endl;
    //        std::cout << "number " << number << std::endl;
    //        ++p[number];
    //    }

    //    std::cout << "a discrete_distribution:" << std::endl;
    //    for (int i=0; i<10; ++i)
    //    {
    //        std::cout << "indexes selected " << p[i] << std::endl;
    //        std::cout << i << ": " << std::string(p[i]*nstars/nrolls,'*') << std::endl;

    //    }

    //    weights_dist_data_.open ("/home/hriday/Desktop/weights.csv", std::ios::out | std::ios::ate | std::ios::app);

    //    for(int i =0; i < num_particles;++i)
    //    {
    //        if(i != num_particles-1)
    //            weights_dist_data_ << "weight no" << i << ",";
    //        else
    //            weights_dist_data_ << "weight no" << i << std::endl;
    //    }
    //    weights_dist_data_.close();

    // ----------------------------------------------------------//

    std::vector<Eigen::VectorXf> init_pose_vec;

    init_pose_vec.resize(num_particles);

    particles_vec_.resize(num_particles);

    weights_.resize(num_particles);

    //inserting the first map element
    arucos_map_.resize(aruco_poses.size());

    for(int i =0; i < aruco_poses.size(); ++i)
    {
        arucos_map_[i].obs_id = i;
        arucos_map_[i].map_pose(0) = aruco_poses[i](0);
        arucos_map_[i].map_pose(1) = aruco_poses[i](1);
        arucos_map_[i].map_pose(2) = aruco_poses[i](2);
        arucos_map_[i].maha_dist   = 0;

        //        arucos_map_[1].obs_id = 1;
        //        arucos_map_[1].map_pose(0) = 3.51;
        //        arucos_map_[1].map_pose(1) =-1.55;
        //        arucos_map_[1].map_pose(2) = 1.38;
        //        arucos_map_[1].maha_dist   = 0;
    }

//    std::cout << "arucos landmark 1 " <<  arucos_map_[0].map_pose << std::endl;
//    std::cout << "arucos landmark 2 " <<  arucos_map_[1].map_pose << std::endl;

    object_map_.resize(mapped_objects.size());

    for(int i=0; i < mapped_objects.size(); ++i)
    {
        object_map_[i].id = i;
        object_map_[i].type = mapped_objects[i].type;
        object_map_[i].pose = mapped_objects[i].pose;
        std::cout << "mapped object pose" <<  object_map_[i].pose << std::endl;
    }

    //normal distribution for init pose 0,0,0. TODO: pass init pose to from the semantic slam classs
    //and also add the std_deviation which is now 0.01
    std::normal_distribution<float> dist_x(0, 0);
    std::normal_distribution<float> dist_y(0, 0);
    std::normal_distribution<float> dist_z(0, 0);
    std::normal_distribution<float> dist_roll(0, 0);
    std::normal_distribution<float> dist_pitch(0, 0);
    std::normal_distribution<float> dist_yaw(0, 0);

    //    std::cout << "printing out the normal dist " << dist_x << std::endl
    //              << dist_y << std::endl
    //              << dist_z << std::endl
    //              << dist_roll << std::endl
    //              << dist_pitch << std::endl
    //              << dist_yaw << std::endl;

    for(int i =0; i < num_particles; ++i)
    {
        weights_[i] = 1;

        init_pose_vec[i].resize(state_size);

        vo_current_pose_.resize(state_size), vo_prev_pose_.resize(state_size);

        //std::cout << "state_vec_kk size " << state_vec_xkk_.size() << std::endl;

        //std::cout << "default random num " << gen << std::endl;

        vo_current_pose_.setZero(); vo_prev_pose_.setZero();

        //        state_vec_xkk_[i](0) = dist_x(gen);
        //        state_vec_xkk_[i](1) = dist_y(gen);
        //        state_vec_xkk_[i](2) = dist_z(gen);
        //        state_vec_xkk_[i](3) = dist_roll(gen);
        //        state_vec_xkk_[i](4) = dist_pitch(gen);
        //        state_vec_xkk_[i](5) = dist_yaw(gen);

        particles_vec_[i].id = i; //id
        particles_vec_[i].x = dist_x(gen); //x
        particles_vec_[i].y = dist_y(gen); //y
        particles_vec_[i].z = dist_z(gen); //z
        particles_vec_[i].roll = dist_roll(gen); //roll
        particles_vec_[i].pitch = dist_pitch(gen); //pitch
        particles_vec_[i].yaw = dist_yaw(gen); //yaw
        particles_vec_[i].weight = 1;  //assigning the weights to 1

        init_pose_vec[i](0) = particles_vec_[i].x;
        init_pose_vec[i](1) = particles_vec_[i].y;
        init_pose_vec[i](2) = particles_vec_[i].z;
        init_pose_vec[i](3) = particles_vec_[i].roll;
        init_pose_vec[i](4) = particles_vec_[i].pitch;
        init_pose_vec[i](5) = particles_vec_[i].yaw;

    }

    jacobian_f_x_.resize(state_size, state_size), VO_prediction_cov_.resize(state_size, state_size), VO_prediction_noise_cov_.resize(state_size, state_size);
    this->computeJacobian();
    this->computePredictionNoiseCov();

    return init_pose_vec;
}

std::vector<Eigen::VectorXf> particle_filter::predictionVO(float deltaT, Eigen::VectorXf vo_pose_world, std::vector<Eigen::VectorXf> filtered_pose, Eigen::VectorXf& final_pose)
{
    //random number generator
    std::default_random_engine gen;

    vo_current_pose_ = vo_pose_world;
    Eigen::VectorXf new_state_vec_xkk;
    new_state_vec_xkk.resize(6), new_state_vec_xkk.setZero();

    for(int i = 0; i < num_particles_; ++i)
    {
        Eigen::VectorXf prev_pose_inv, pose_increment;
        Eigen::VectorXf current_pose;
        prev_pose_inv.resize(6),  prev_pose_inv.setZero();
        pose_increment.resize(6), pose_increment.setZero();
        current_pose.resize(6), current_pose.setZero();


        //----------------------This method is using a model---------------------------//

        //        std::normal_distribution<float> noise_x(0, 0.01);
        //        std::normal_distribution<float> noise_y(0, 0.01);
        //        std::normal_distribution<float> noise_z(0, 0.01);
        //        new_state_vec_xkk(0) = filtered_pose[i](0) + noise_x(gen);  //x = prevx +noise
        //        new_state_vec_xkk(1) = filtered_pose[i](1) + noise_y(gen);  //y = prevy +noise
        //        new_state_vec_xkk(2) = filtered_pose[i](2) + noise_z(gen);  //z = prevz +noise
        //        new_state_vec_xkk(3) = filtered_pose[i](3);           //roll = prevroll
        //        new_state_vec_xkk(4) = filtered_pose[i](4);           //pitch = prevpitch
        //        new_state_vec_xkk(5) = filtered_pose[i](5);           //yaw = prevyaw


        //        filtered_pose[i](0) = new_state_vec_xkk(0);
        //        filtered_pose[i](1) = new_state_vec_xkk(1);
        //        filtered_pose[i](2) = new_state_vec_xkk(2) ;
        //        filtered_pose[i](3) = new_state_vec_xkk(3);
        //        filtered_pose[i](4) = new_state_vec_xkk(4);
        //        filtered_pose[i](5) = new_state_vec_xkk(5);

        //----------------------------------------------------------------------------//

        //----------------------this method is using the VO pose----------------------//

        //        //compute the inv of the prev pose
        prev_pose_inv = this->computeVectorInv(vo_prev_pose_);
        //std::cout << "prev_pose_inv " << prev_pose_inv << std::endl;

        //compute the pose increment
        pose_increment = this->computeDiffUsingHomoMethod(prev_pose_inv, vo_current_pose_);
        //std::cout << "pose increment " << pose_increment << std::endl;

        //compute the pose using the prediction
        new_state_vec_xkk = this->computeDiffUsingHomoMethod(filtered_pose[i], pose_increment);
        //std::cout << "new_state_vec_xkk " << " " << i << " " << new_state_vec_xkk << std::endl;


        //        std::normal_distribution<float> noise_x(0, 0.1);
        //        std::normal_distribution<float> noise_y(0, 0.1);
        //        std::normal_distribution<float> noise_z(0, 0.1);

        //adding gaussian noise to each normal distribution for each VO measurement
        std::normal_distribution<float> dist_x(new_state_vec_xkk(0), 0.03);
        std::normal_distribution<float> dist_y(new_state_vec_xkk(1), 0.03);
        std::normal_distribution<float> dist_z(new_state_vec_xkk(2), 0.03);
        std::normal_distribution<float> dist_roll(new_state_vec_xkk(3), 0.03);
        std::normal_distribution<float> dist_pitch(new_state_vec_xkk(4), 0.03);
        std::normal_distribution<float> dist_yaw(new_state_vec_xkk(5), 0.03);

        //updating each particle value based on the normal distribution
        filtered_pose[i].setZero();
        filtered_pose[i](0) = dist_x(gen);
        filtered_pose[i](1) = dist_y(gen);
        filtered_pose[i](2) = dist_z(gen);
        filtered_pose[i](3) = dist_roll(gen);
        filtered_pose[i](4) = dist_pitch(gen);
        filtered_pose[i](5) = dist_yaw(gen);

        //--------------------------------------------------------------------------------------//

    }

    vo_prev_pose_ = vo_current_pose_;

    Eigen::VectorXf avg_pose;
    avg_pose.resize(state_size_), avg_pose.setZero();
    for(int i = 0; i < num_particles_; ++i)
    {
        avg_pose(0) += filtered_pose[i](0);
        avg_pose(1) += filtered_pose[i](1);
        avg_pose(2) += filtered_pose[i](2);
        avg_pose(3) += filtered_pose[i](3);
        avg_pose(4) += filtered_pose[i](4);
        avg_pose(5) += filtered_pose[i](5);

    }

    avg_pose(0) = avg_pose(0)/num_particles_;
    avg_pose(1) = avg_pose(1)/num_particles_;
    avg_pose(2) = avg_pose(2)/num_particles_;
    avg_pose(3) = avg_pose(3)/num_particles_;
    avg_pose(4) = avg_pose(4)/num_particles_;
    avg_pose(5) = avg_pose(5)/num_particles_;


    final_pose = avg_pose;

    return filtered_pose;
}

std::vector<Eigen::VectorXf> particle_filter::bebopIMUUpdate(float roll, float pitch, float yaw, std::vector<Eigen::VectorXf> filtered_pose, Eigen::VectorXf& final_pose)
{

    std::vector<float> z_est;
    Eigen::VectorXf innovation_vec;
    std::vector<Eigen::VectorXf> resampled_state;
    std::default_random_engine gen;

    weights_.clear();
    weights_.resize(num_particles_);

    for(int i =0; i < num_particles_; ++i)
    {
        weights_[i] = 1;
    }

    z_est.clear(); z_est.resize(3);
    innovation_vec.resize(3);

    double sum =0;
    for (int i = 0; i < num_particles_; ++i)
    {
        z_est[0] = filtered_pose[i](3);  //roll
        z_est[1] = filtered_pose[i](4);  //pitch
        z_est[2] = filtered_pose[i](5);  //yaw

        innovation_vec(0) = roll  - z_est[0];
        innovation_vec(1) = pitch - z_est[1];

        //normalization of yaw is required
        innovation_vec(2) = yaw   - z_est[2];
        if(fabs(innovation_vec(2))>M_PI)
        {
            if(innovation_vec(2)>0)
                innovation_vec(2)-=(2*M_PI);
            else
                innovation_vec(2)+=(2*M_PI);
        }



        //std::cout << "innovation of the bebop imu " << innovation_vec << std::endl;

        float sigmax = 1000, sigmay = 1000, sigmaz = 1000;
        float varx = sigmax * sigmax;
        float vary = sigmay * sigmay;
        float varz = sigmaz * sigmaz;

        float num1 = exp(-1 * (pow(innovation_vec(0), 2) / 2*sigmax));
        float num2 = exp(-1 * (pow(innovation_vec(1), 2) / 2*sigmay));
        float num3 = exp(-1 * (pow(innovation_vec(2), 2) / 2*sigmaz));

        //double den = 0.5 / M_PI / sigmax / sigmay / sigmaz;
        float den = sqrt(2 * M_PI * sigmax * sigmay * sigmaz);

        //den *= exp(num1 + num2 + num3);

        weights_[i] *= ((num1+num2+num3)/den);
        //std::cout << "weight " << i << " " << weights_[i] << std::endl;
        sum += weights_[i];

        particles_vec_[i].id     = i;
        particles_vec_[i].x      = filtered_pose[i](0);
        particles_vec_[i].y      = filtered_pose[i](1);
        particles_vec_[i].z      = filtered_pose[i](2);
        particles_vec_[i].roll   = filtered_pose[i](3);
        particles_vec_[i].pitch  = filtered_pose[i](4);
        particles_vec_[i].yaw    = filtered_pose[i](5);
        particles_vec_[i].weight = weights_[i];


    }


    for(int l=0; l<num_particles_; ++l)
    {
        weights_[l] = weights_[l]/sum;
        //std::cout << "weights normalized " << weights_[l] << std::endl;

        particles_vec_[l].weight = weights_[l];
        //std::cout << "weights normalized in the struct  " << particles_vec_[l].weight << std::endl;

    }

    //resampling using discrite distribution

    //sorting the weight from high to low number
    std::vector<float> sorted_weights;
    sorted_weights = weights_;

    std::sort(sorted_weights.begin(), sorted_weights.end(), highToLow);

    //sort all the particles based on their weights from high to low
    std::sort(particles_vec_.begin(), particles_vec_.end(), particlesHighToLow);

    std::discrete_distribution<int> distr(sorted_weights.begin(), sorted_weights.end());

    resampled_state.resize(num_particles_);
    for(int c = 0; c < sorted_weights.size(); ++c)
    {
        int number = distr(gen);
        //std::cout << "index  " << number << std::endl;
        resampled_state[c].resize(state_size_);

        //This stage we update only the r,y and y angles of the particles and keep the x,y and z the same as they will be updated by the arucos
        resampled_state[c](0) = particles_vec_[c].x;
        resampled_state[c](1) = particles_vec_[c].y;
        resampled_state[c](2) = particles_vec_[c].z;
        resampled_state[c](3) = particles_vec_[number].roll;
        resampled_state[c](4) = particles_vec_[number].pitch;
        resampled_state[c](5) = particles_vec_[number].yaw;

    }

    filtered_pose.clear(); filtered_pose.resize(resampled_state.size());
    filtered_pose = resampled_state;

    Eigen::VectorXf avg_pose;
    avg_pose.resize(state_size_), avg_pose.setZero();
    for(int i = 0; i < num_particles_; ++i)
    {
        avg_pose(0) += filtered_pose[i](0);
        avg_pose(1) += filtered_pose[i](1);
        avg_pose(2) += filtered_pose[i](2);
        avg_pose(3) += filtered_pose[i](3);
        avg_pose(4) += filtered_pose[i](4);
        avg_pose(5) += filtered_pose[i](5);

    }

    avg_pose(0) = avg_pose(0)/num_particles_;
    avg_pose(1) = avg_pose(1)/num_particles_;
    avg_pose(2) = avg_pose(2)/num_particles_;
    avg_pose(3) = avg_pose(3)/num_particles_;
    avg_pose(4) = avg_pose(4)/num_particles_;
    avg_pose(5) = avg_pose(5)/num_particles_;

    final_pose = avg_pose;

    //    //this is finding the mahalonobis distance
    //    VO_prediction_cov_ = jacobian_f_x_*VO_prediction_cov_*jacobian_f_x_.transpose().eval() + VO_prediction_noise_cov_;

    return filtered_pose;

}


//add the projection of the detected arucos in the image,
//take the inverse of the average pose and then multiply with the actual pose of the aruco
//then use the pin hole camera model to put this this derived 3D point in image coordinates
std::vector<Eigen::VectorXf> particle_filter::arucoMapAndUpdate(std::vector<Eigen::Vector4f> aruco_pose, std::vector<Eigen::VectorXf> filtered_pose, Eigen::VectorXf &final_pose, Eigen::VectorXf VO_pose)
{
    std::vector<float> z_map_est;
    Eigen::VectorXf innovation_vec;
    Eigen::MatrixXf innovation_cov;
    innovation_cov.resize(3, 3);
    std::vector<Eigen::VectorXf> resampled_state;
    weights_.clear();
    weights_.resize(num_particles_);
    std::default_random_engine gen;

    std::vector<Eigen::Vector4f> aruco_pose_world, aruco_pose_robot;
    aruco_pose_world.clear(); aruco_pose_world.resize(aruco_pose.size());
    aruco_pose_robot.clear(); aruco_pose_robot.resize(aruco_pose.size());

    for(int i =0; i < num_particles_; ++i)
    {
        weights_[i] = 1;
    }

    z_map_est.resize(3), innovation_vec.resize(3), resampled_state.clear();

    for(int i = 0; i < aruco_pose.size(); ++i)
    {
        for (int j =0; j < arucos_map_.size(); ++j)
        {
            float sum = 0;
            if(aruco_pose[i](3) == arucos_map_[j].obs_id)
            {
                Eigen::Matrix4f rotation_mat;
                for (int k =0; k < num_particles_; ++k)
                {
                    //the aruco pose needs to be converted to world frame, currently it is in robot
                    //converting the aruco pose to world using the estimated roll, pitch and yaw of all the particles
                    this->transfromRobotToWorldFrame(filtered_pose[k](3), filtered_pose[k](4), filtered_pose[k](5), rotation_mat);

                            //assing first the aruco pose to aruco_pose_robot vec
                            aruco_pose_robot[i](0) = aruco_pose[i](0);
                    aruco_pose_robot[i](1) = aruco_pose[i](1);
                    aruco_pose_robot[i](2) = aruco_pose[i](2);
                    aruco_pose_robot[i](3) = 1;

                    aruco_pose_world[i] = rotation_mat * aruco_pose_robot[i];

                    std::cout << "aruco pose world frame: " << aruco_pose_world[i] << std::endl;
                    std::cout << "aruco pose robot frame: " << aruco_pose[i] << std::endl;

                    //                    z_map_est[0] = aruco_pose[i](0) + filtered_pose[k](0);
                    //                    z_map_est[1] = aruco_pose[i](1) + filtered_pose[k](1);
                    //                    z_map_est[2] = aruco_pose[i](2) + filtered_pose[k](2);

                    //Adding the converted aruco pose in world frame
                    z_map_est[0] = aruco_pose_world[i](0) + filtered_pose[k](0);
                    z_map_est[1] = aruco_pose_world[i](1) + filtered_pose[k](1);
                    z_map_est[2] = aruco_pose_world[i](2) + filtered_pose[k](2);


                    //for(int z = 0; z < z_map_est.size(); ++z)
                    //{
                    //std::cout << "aruco pose estimated " << z_map_est[z] << std::endl;

                    //}
                    std::cout << "corresponding particle pose " << filtered_pose[k] << std::endl;

                    innovation_vec(0) = (arucos_map_[j].map_pose(0) - z_map_est[0]);
                    innovation_vec(1) = (arucos_map_[j].map_pose(1) - z_map_est[1]);
                    innovation_vec(2) = (arucos_map_[j].map_pose(2) - z_map_est[2]);

                    //std::cout << "innovation vec " << innovation_vec << std::endl;
                    float sigmax = 100, sigmay = 100, sigmaz = 100;
                    float varx = sigmax * sigmax;
                    float vary = sigmay * sigmay;
                    float varz = sigmaz * sigmaz;

                    float num1 = exp(-1 * (pow(innovation_vec(0), 2) / 2*sigmax));
                    float num2 = exp(-1 * (pow(innovation_vec(1), 2) / 2*sigmay));
                    float num3 = exp(-1 * (pow(innovation_vec(2), 2) / 2*sigmaz));

                    //double den = 0.5 / M_PI / sigmax / sigmay / sigmaz;
                    float den = sqrt(2 * M_PI * sigmax * sigmay * sigmaz);

                    //den *= exp(num1 + num2 + num3);

                    weights_[k] *= ((num1+num2+num3)/den);
                    std::cout << "weight " << k << " " << weights_[k] << std::endl;
                    sum += weights_[k];

                    particles_vec_[k].id     = k;
                    particles_vec_[k].x      = filtered_pose[k](0);
                    particles_vec_[k].y      = filtered_pose[k](1);
                    particles_vec_[k].z      = filtered_pose[k](2);
                    particles_vec_[k].roll   = filtered_pose[k](3);
                    particles_vec_[k].pitch  = filtered_pose[k](4);
                    particles_vec_[k].yaw    = filtered_pose[k](5);
                    particles_vec_[k].weight = weights_[k];


                }

                for(int l=0; l<num_particles_; ++l)
                {
                    weights_[l] = weights_[l]/sum;
                    //std::cout << "weights normalized " << weights_[l] << std::endl;

                    particles_vec_[l].weight = weights_[l];
                    //std::cout << "weights normalized in the struct  " << particles_vec_[l].weight << std::endl;

                }

                //resampling using the cumsum method
                //                std::vector<float> cumsum_weights;
                //                cumsum_weights.resize(weights_.size());

                //                auto cumsum = std::partial_sum(weights_.begin(), weights_.end(), cumsum_weights.begin());

                //                //for(int c = 0; c < cumsum_weights.size(); ++c)
                //                // std::cout << "cumsum of the the weights " << cumsum_weights[c] << std::endl;

                //                for(int p = 0; p < num_particles_; ++p)
                //                {
                //                    float random_number = ((float) rand() / (RAND_MAX));
                //                    //std::cout << "random number " << random_number << std::endl;
                //                    for (int c = 0; c < cumsum_weights.size(); ++c)
                //                    {
                //                        if(random_number < cumsum_weights[c])
                //                        {
                //                            //std::cout << "The index for resampling the particle is " << c << std::endl;
                //                            resampled_state.push_back(filtered_pose[c]);
                //                            //std::cout << "resampled state " << filtered_pose[c] << std::endl;
                //                            break;
                //                        }
                //                    }

                //                }



                //resampling using discrite distribution

                //sorting the weight from high to low number
                std::vector<float> sorted_weights;
                sorted_weights = weights_;

                std::sort(sorted_weights.begin(), sorted_weights.end(), highToLow);

                //                std::cout << "first highest weight index " << std::distance(weights_.begin(), (std::find(weights_.begin(), weights_.end(), sorted_weights[0]))) << std::endl;
                //                std::cout << "first highest weight " << *std::find(weights_.begin(), weights_.end(), sorted_weights[0])  << std::endl;
                //                std::cout << "first highest weight pose " << filtered_pose[std::distance(weights_.begin(), (std::find(weights_.begin(), weights_.end(), sorted_weights[0])))]  << std::endl;


                //                for(int w =0; w<num_particles_; ++w)
                //                {
                //                    std::cout << "weights sorted " << sorted_weights[w] << std::endl;
                //                }

                //sort all the particles based on their weights from high to low
                std::sort(particles_vec_.begin(), particles_vec_.end(), particlesHighToLow);

                //                for(int p = 0; p<num_particles_; ++p)
                //                {
                //                    std::cout << "sorted particles according to their weights " << particles_vec_[p].weight << " "
                //                              << "x: " << particles_vec_[p].x
                //                              << "y: " << particles_vec_[p].y
                //                              << "z: " << particles_vec_[p].z << std::endl;
                //                }

                std::discrete_distribution<int> distr(sorted_weights.begin(), sorted_weights.end());
                //weights_dist_data_.open("/home/hriday/Desktop/weights.csv", std::ios::out | std::ios::ate | std::ios::app) ;

                resampled_state.resize(num_particles_);
                for(int c = 0; c < sorted_weights.size(); ++c)
                {
                    int number = distr(gen);
                    //std::cout << "index  " << number << std::endl;
                    resampled_state[c].resize(state_size_);

                    //In this stage we update only the x,y,z of the particles and keep the r,p and y the same as it is updated
                    //from the IMU
                    resampled_state[c](0) = particles_vec_[number].x;
                    resampled_state[c](1) = particles_vec_[number].y;
                    resampled_state[c](2) = particles_vec_[number].z;
                    resampled_state[c](3) = particles_vec_[c].roll;
                    resampled_state[c](4) = particles_vec_[c].pitch;
                    resampled_state[c](5) = particles_vec_[c].yaw;

                    //                    if (c != num_particles_ - 1){
                    //                        weights_dist_data_ << weights_[c] << ",";
                    //                    }
                    //                    else{
                    //                        weights_dist_data_ << weights_[c] << std::endl;
                    //                    }


                }

                filtered_pose.clear(); filtered_pose.resize(resampled_state.size());
                filtered_pose = resampled_state;


                //              float p[sorted_weights.size()]={};

                //                for (int t =0; t < sorted_weights.size(); ++t)
                //                {
                //                    int number = distr(gen);
                //                    std::cout << "number  " << number << std::endl;
                //                    ++p[number];
                //                }

                //                std::cout << "a discrete_distribution:" << std::endl;
                //                for (int index=0; index<sorted_weights.size(); ++index)
                //                {
                //                    std::cout << "indexes selected " << p[index] << std::endl;
                //                    //std::cout << i << ": " << std::string(p[i]*nstars/nrolls,'*') << std::endl;

                //                }

                //this is just for plotting the weights
                //                weights_dist_data_.close();
                //                iteration_num_++;

                //resampling my method
                //sorting the weight from high to low number
                //                std::vector<float> sorted_weights;
                //                sorted_weights = weights_;

                //                std::sort(sorted_weights.begin(), sorted_weights.end(), highToLow);

                //                //                for(int i=0; i < weights_.size(); ++i)
                //                //                    std::cout << "weights sorted " << weights_[i] << std::endl;

                //                //selecting the first 2 weights and finding their index
                //                //                for(int i=0; i < 2; ++i)
                //                //                    std::cout << "weight indexes " << std::distance(weights_.begin(), (std::find(weights_.begin(), weights_.end(), sorted_weights[i]))) << std::endl;


                //                std::cout << "first highest weight index " << std::distance(weights_.begin(), (std::find(weights_.begin(), weights_.end(), sorted_weights[0]))) << std::endl;
                //                std::cout << "first highest weight " << *std::find(weights_.begin(), weights_.end(), sorted_weights[0])  << std::endl;
                //                std::cout << "second highest weight index " << std::distance(weights_.begin(), (std::find(weights_.begin(), weights_.end(), sorted_weights[1]))) << std::endl;
                //                std::cout << "second highest weight " << *std::find(weights_.begin(), weights_.end(), sorted_weights[1]) << std::endl;

                //                //selectin the highest first 50 particles
                //                for(int i= 0; i < 50; ++i)
                //                {
                //                    for(int j =0; j < (num_particles_/50); ++j)
                //                    {
                //                        resampled_state.push_back(filtered_pose[std::distance(weights_.begin(), (std::find(weights_.begin(), weights_.end(), sorted_weights[i])))]);
                //                    }

                //                }

            }

        }

    }

    //    for(int i = 0; i < aruco_pose.size(); ++i)
    //    {
    //        for(int j = 0; j < arucos_map_.size(); ++j)
    //        {
    //            if(aruco_pose[i](3) == arucos_map_[j].obs_id)
    //            {
    //                filtered_pose.clear(); filtered_pose.resize(resampled_state.size());
    //                filtered_pose = resampled_state;
    //            }
    //            //    final_pose = filtered_pose[best_weight];

    //        }
    //    }

    Eigen::VectorXf avg_pose;
    avg_pose.resize(state_size_), avg_pose.setZero();
    for(int i = 0; i < num_particles_; ++i)
    {
        avg_pose(0) += filtered_pose[i](0);
        avg_pose(1) += filtered_pose[i](1);
        avg_pose(2) += filtered_pose[i](2);
        avg_pose(3) += filtered_pose[i](3);
        avg_pose(4) += filtered_pose[i](4);
        avg_pose(5) += filtered_pose[i](5);

    }

    avg_pose(0) = avg_pose(0)/num_particles_;
    avg_pose(1) = avg_pose(1)/num_particles_;
    avg_pose(2) = avg_pose(2)/num_particles_;
    avg_pose(3) = avg_pose(3)/num_particles_;
    avg_pose(4) = avg_pose(4)/num_particles_;
    avg_pose(5) = avg_pose(5)/num_particles_;


    final_pose = avg_pose;
    std::cout << "avg_pose  " << avg_pose << std::endl;
    std::cout << "corresponding VO_pose " << VO_pose  << std::endl;

    return filtered_pose;


}

std::vector<Eigen::VectorXf> particle_filter::ObjectMapAndUpdate(object_info_struct_pf complete_objec_info, std::vector<Eigen::VectorXf> filtered_pose, Eigen::VectorXf &final_pose, Eigen::VectorXf VO_pose)
{

    //The variance will be assigned based on the number of the points obtained from the segmented plane
    float sigmax, sigmay, sigmaz;
    if(complete_objec_info.num_points > 500)
        sigmax = 1000, sigmay = 1000, sigmaz = 1000;
    else if(complete_objec_info.num_points > 100 && complete_objec_info.num_points < 500)
        sigmax = 10, sigmay = 10, sigmaz = 10;
    else if(complete_objec_info.num_points > 50 &&  complete_objec_info.num_points < 100)
        sigmax = 0.1, sigmay = 0.1, sigmaz = 0.1;
    else if (complete_objec_info.num_points < 50)
    {
        std::cout << "\033[1;31m Not updating as the 3D points in the segmented plane are less than 50 \033[0m" << std::endl;
        return filtered_pose;
    }

    std::vector<float> z_map_est;
    Eigen::VectorXf innovation_vec;
    Eigen::MatrixXf innovation_cov;
    innovation_cov.resize(3, 3);
    std::vector<Eigen::VectorXf> resampled_state;
    weights_.clear();
    weights_.resize(num_particles_);
    std::default_random_engine gen;

    for(int i =0; i < num_particles_; ++i)
    {
        weights_[i] = 1;
    }

    float sum = 0;
    z_map_est.resize(3), innovation_vec.resize(3), resampled_state.clear();


    //calculate the diff of matching the detected object with the mapped objects
    std::vector<float> avg_diff_vec;
    float avg_diff;
    for(int i = 0; i < object_map_.size(); ++i)
    {
        float x_diff = 0, y_diff =0 , z_diff=0;

        if(object_map_[i].type == complete_objec_info.type )
        {

            x_diff = fabs(object_map_[i].pose(0) - (complete_objec_info.pose(0) + final_pose(0)));
            y_diff = fabs(object_map_[i].pose(1) - (complete_objec_info.pose(1) + final_pose(1)));
            z_diff = fabs(object_map_[i].pose(2) - (complete_objec_info.pose(2) + final_pose(2)));

            std::cout << "object pose in map x " << object_map_[i].pose(0) << std::endl;
            std::cout << "complete_objec_info pose x " << complete_objec_info.pose(0) << std::endl;
            std::cout << "final pose x  " << final_pose(0) << std::endl;

            std::cout << "object pose in map y " << object_map_[i].pose(1) << std::endl;
            std::cout << "complete_objec_info pose y " << complete_objec_info.pose(1) << std::endl;
            std::cout << "final pose y  " << final_pose(1) << std::endl;

            std::cout << "object pose in map z " << object_map_[i].pose(2) << std::endl;
            std::cout << "complete_objec_info pose z " << complete_objec_info.pose(2) << std::endl;
            std::cout << "final pose z  " << final_pose(2) << std::endl;


            std::cout << "x_diff " <<  x_diff << std::endl;
            std::cout << "y_diff " <<  y_diff << std::endl;
            std::cout << "z_diff " <<  z_diff << std::endl;

            avg_diff = (x_diff + y_diff + z_diff) / 2;

            //            if(x_diff > 2.0 || y_diff > 2.0)
            //            {

            //                if(complete_objec_info.num_points < 300)
            //                {
            //                    std::cout << "\033[1;31m returning as no new chair found for matching  \033[0m" << std::endl;
            //                    return filtered_pose;
            //                }
            //                else if (complete_objec_info.num_points > 300)
            //                {
            //                    std::cout << "\033[1;32m Adding a new detected chair \033[0m" << std::endl;
            //                    object_info_struct_pf obj_info_for_adding;

            //                    obj_info_for_adding.id = object_map_.size();
            //                    obj_info_for_adding.pose(0) = complete_objec_info.pose(0) + final_pose(0);
            //                    obj_info_for_adding.pose(1) = complete_objec_info.pose(1) + final_pose(1);
            //                    obj_info_for_adding.pose(2) = complete_objec_info.pose(2) + final_pose(2);
            //                    obj_info_for_adding.type    = complete_objec_info.type;

            //                    object_map_.push_back(obj_info_for_adding);

            //                    std::cout << "New object added at pose: " << obj_info_for_adding.pose << std::endl;
            //                    return filtered_pose;
            //                }
            //            }

        }
        else
        {
            avg_diff = 1000;
        }

        avg_diff_vec.push_back(avg_diff);
    }

    //This min element is the id of the matched mapped object with the current detected object
    auto min_element = *std::min_element(avg_diff_vec.begin(), avg_diff_vec.end(), comparatorlowToHigh);
    std::cout << "min element " << min_element << std::endl;

    if(min_element >= 1.1)
    {
        std::cout << "\033[1;31m Not updating as the avg diff is too high and cannot match with the corresponding object \033[0m " << std::endl;
        return filtered_pose;
    }

    auto min_element_id = std::distance(avg_diff_vec.begin(), (std::min_element(avg_diff_vec.begin(), avg_diff_vec.end(), comparatorlowToHigh)));
    std::cout << "matched object id " << min_element_id << std::endl;

    for (int i =0; i < num_particles_; ++i)
    {
        //Adding the object pose in robot frame to world frame
        z_map_est[0] = complete_objec_info.pose(0) + filtered_pose[i](0);
        z_map_est[1] = complete_objec_info.pose(1) + filtered_pose[i](1);
        z_map_est[2] = complete_objec_info.pose(2) + filtered_pose[i](2);

        //std::cout << "corresponding particle pose " << filtered_pose[i] << std::endl;

        innovation_vec(0) = (object_map_[min_element_id].pose(0) - z_map_est[0]);
        innovation_vec(1) = (object_map_[min_element_id].pose(1) - z_map_est[1]);
        innovation_vec(2) = (object_map_[min_element_id].pose(2) - z_map_est[2]);

        //std::cout << "innovation vec " << innovation_vec << std::endl;
        float varx = sigmax * sigmax;
        float vary = sigmay * sigmay;
        float varz = sigmaz * sigmaz;

        float num1 = exp(-1 * (pow(innovation_vec(0), 2) / 2*sigmax));
        float num2 = exp(-1 * (pow(innovation_vec(1), 2) / 2*sigmay));
        float num3 = exp(-1 * (pow(innovation_vec(2), 2) / 2*sigmaz));

        //double den = 0.5 / M_PI / sigmax / sigmay / sigmaz;
        float den = sqrt(2 * M_PI * sigmax * sigmay * sigmaz);

        //den *= exp(num1 + num2 + num3);

        weights_[i] *= ((num1+num2+num3)/den);
        //std::cout << "weight " << i << " " << weights_[i] << std::endl;
        sum += weights_[i];

        particles_vec_[i].id     = i;
        particles_vec_[i].x      = filtered_pose[i](0);
        particles_vec_[i].y      = filtered_pose[i](1);
        particles_vec_[i].z      = filtered_pose[i](2);
        particles_vec_[i].roll   = filtered_pose[i](3);
        particles_vec_[i].pitch  = filtered_pose[i](4);
        particles_vec_[i].yaw    = filtered_pose[i](5);
        particles_vec_[i].weight = weights_[i];

    }

    for(int l=0; l<num_particles_; ++l)
    {
        weights_[l] = weights_[l]/sum;
        //std::cout << "weights normalized " << weights_[l] << std::endl;

        particles_vec_[l].weight = weights_[l];
        //std::cout << "weights normalized in the struct  " << particles_vec_[l].weight << std::endl;

    }

    //sorting the weight from high to low number
    std::vector<float> sorted_weights;
    sorted_weights = weights_;

    std::sort(sorted_weights.begin(), sorted_weights.end(), highToLow);

    //sort all the particles based on their weights from high to low
    std::sort(particles_vec_.begin(), particles_vec_.end(), particlesHighToLow);

    std::discrete_distribution<int> distr(sorted_weights.begin(), sorted_weights.end());
    //weights_dist_data_.open("/home/hriday/Desktop/weights.csv", std::ios::out | std::ios::ate | std::ios::app) ;

    resampled_state.resize(num_particles_);
    for(int c = 0; c < sorted_weights.size(); ++c)
    {
        int number = distr(gen);
        //std::cout << "index  " << number << std::endl;
        resampled_state[c].resize(state_size_);

        //In this stage we update only the x,y,z of the particles and keep the r,p and y the same as it is updated
        //from the IMU
        resampled_state[c](0) = particles_vec_[number].x;
        resampled_state[c](1) = particles_vec_[number].y;
        resampled_state[c](2) = particles_vec_[number].z;
        resampled_state[c](3) = particles_vec_[c].roll;
        resampled_state[c](4) = particles_vec_[c].pitch;
        resampled_state[c](5) = particles_vec_[c].yaw;
    }

    filtered_pose.clear(); filtered_pose.resize(resampled_state.size());
    filtered_pose = resampled_state;

    Eigen::VectorXf avg_pose;
    avg_pose.resize(state_size_), avg_pose.setZero();
    for(int i = 0; i < num_particles_; ++i)
    {
        avg_pose(0) += filtered_pose[i](0);
        avg_pose(1) += filtered_pose[i](1);
        avg_pose(2) += filtered_pose[i](2);
        avg_pose(3) += filtered_pose[i](3);
        avg_pose(4) += filtered_pose[i](4);
        avg_pose(5) += filtered_pose[i](5);

    }

    avg_pose(0) = avg_pose(0)/num_particles_;
    avg_pose(1) = avg_pose(1)/num_particles_;
    avg_pose(2) = avg_pose(2)/num_particles_;
    avg_pose(3) = avg_pose(3)/num_particles_;
    avg_pose(4) = avg_pose(4)/num_particles_;
    avg_pose(5) = avg_pose(5)/num_particles_;


    final_pose = avg_pose;
    //std::cout << "avg_pose  " << avg_pose << std::endl;
    //std::cout << "corresponding VO_pose " << VO_pose  << std::endl;


    return filtered_pose;

}


std::vector<Eigen::VectorXf> particle_filter::slamdunkPoseUpdate(Eigen::Vector3f slamdunk_pose, std::vector<Eigen::VectorXf> filtered_pose, Eigen::VectorXf VO_pose)
{
    Eigen::Vector3f innovation_vec;
    innovation_vec.setZero();
    weights_.clear();
    weights_.resize(num_particles_);
    std::vector<Eigen::VectorXf> resampled_state;
    float sum =0;
    std::default_random_engine gen;

    for(int i =0; i < num_particles_; ++i)
    {
        weights_[i] = 1;
    }

    for(int i =0; i < num_particles_; ++i)
    {
        innovation_vec(0) = filtered_pose[i](0) - slamdunk_pose(0);
        innovation_vec(1) = filtered_pose[i](1) - slamdunk_pose(1);
        innovation_vec(2) = filtered_pose[i](2) - slamdunk_pose(2);

        std::cout << "innovation vec " << innovation_vec << std::endl;
        float sigmax = 100, sigmay = 100, sigmaz = 100;
        float varx = sigmax * sigmax;
        float vary = sigmay * sigmay;
        float varz = sigmaz * sigmaz;

        float num1 = exp(-0.5 * (pow(innovation_vec(0), 2) / 2*varx));
        float num2 = exp(-0.5 * (pow(innovation_vec(1), 2) / 2*vary));
        float num3 = exp(-0.5 * (pow(innovation_vec(2), 2) / 2*varz));

        //double den = 0.5 / M_PI / sigmax / sigmay / sigmaz;
        float den = 2 * M_PI * sigmax * sigmay * sigmaz;

        //den *= exp(num1 + num2 + num3);

        weights_[i] *= ((num1+num2+num3)/den);
        std::cout << "weight " << i << " " << weights_[i] << std::endl;
        sum += weights_[i];

        particles_vec_[i].id     = i;
        particles_vec_[i].x      = filtered_pose[i](0);
        particles_vec_[i].y      = filtered_pose[i](1);
        particles_vec_[i].z      = filtered_pose[i](2);
        particles_vec_[i].roll   = filtered_pose[i](3);
        particles_vec_[i].pitch  = filtered_pose[i](4);
        particles_vec_[i].yaw    = filtered_pose[i](5);
        particles_vec_[i].weight = weights_[i];


    }

    for(int l=0; l<num_particles_; ++l)
    {
        weights_[l] = weights_[l]/sum;
        //std::cout << "weights normalized " << weights_[l] << std::endl;
        particles_vec_[l].weight = weights_[l];
        //std::cout << "weights normalized in the struct  " << particles_vec_[l].weight << std::endl;
    }


    //--------------------------------resampling using discrite distribution ----------------------------------//

    //sorting the weights from high to low number
    std::vector<float> sorted_weights;
    sorted_weights = weights_;

    std::sort(sorted_weights.begin(), sorted_weights.end(), highToLow);

    //    std::cout << "first highest weight index " << std::distance(weights_.begin(), (std::find(weights_.begin(), weights_.end(), sorted_weights[0]))) << std::endl;
    //    std::cout << "first highest weight " << *std::find(weights_.begin(), weights_.end(), sorted_weights[0])  << std::endl;
    //    std::cout << "first highest weight pose " << filtered_pose[std::distance(weights_.begin(), (std::find(weights_.begin(), weights_.end(), sorted_weights[0])))]  << std::endl;


    //sort all the particles based on their weights from high to low
    std::sort(particles_vec_.begin(), particles_vec_.end(), particlesHighToLow);

    //    for(int p = 0; p<num_particles_; ++p)
    //    {
    //        std::cout << "sorted particles according to their weights " << particles_vec_[p].weight << " "
    //                  << "x: " << particles_vec_[p].x
    //                  << "y: " << particles_vec_[p].y
    //                  << "z: " << particles_vec_[p].z << std::endl;
    //    }

    //descrete distribution using the sorted weights
    std::discrete_distribution<int> distr(sorted_weights.begin(), sorted_weights.end());

    resampled_state.resize(num_particles_);
    for(int c = 0; c < sorted_weights.size(); ++c)
    {
        int number = distr(gen);
        //std::cout << "index  " << number << std::endl;
        resampled_state[c].resize(state_size_);

        resampled_state[c](0) = particles_vec_[number].x;
        resampled_state[c](1) = particles_vec_[number].y;
        resampled_state[c](2) = particles_vec_[number].z;
        resampled_state[c](3) = particles_vec_[number].roll;
        resampled_state[c](4) = particles_vec_[number].pitch;
        resampled_state[c](5) = particles_vec_[number].yaw;

    }

    //-----------------------------------------------------------------------------------------------------------//

    //resampling using the cumsum method
    //    std::vector<float> cumsum_weights;
    //    cumsum_weights.resize(weights_.size());

    //    auto cumsum = std::partial_sum(weights_.begin(), weights_.end(), cumsum_weights.begin());

    //    //for(int c = 0; c < cumsum_weights.size(); ++c)
    //    // std::cout << "cumsum of the the weights " << cumsum_weights[c] << std::endl;

    //    for(int p = 0; p < num_particles_; ++p)
    //    {
    //        float random_number = ((float) rand() / (RAND_MAX));
    //        //std::cout << "random number " << random_number << std::endl;
    //        for (int c = 0; c < cumsum_weights.size(); ++c)
    //        {
    //            if(random_number < cumsum_weights[c])
    //            {
    //                //std::cout << "The index for resampling the particle is " << c << std::endl;
    //                resampled_state.push_back(filtered_pose[c]);
    //                //std::cout << "resampled state " << filtered_pose[c] << std::endl;
    //                break;
    //            }
    //        }

    //    }

    //resampling my method
    //    //sorting the weight from high to low number
    //    std::vector<float> sorted_weights;
    //    sorted_weights = weights_;

    //    std::sort(sorted_weights.begin(), sorted_weights.end(), highToLow);

    //    //                for(int i=0; i < weights_.size(); ++i)
    //    //                    std::cout << "weights sorted " << weights_[i] << std::endl;

    //    std::cout << "first highest weight index " << std::distance(weights_.begin(), (std::find(weights_.begin(), weights_.end(), sorted_weights[0]))) << std::endl;
    //    std::cout << "first highest weight " << *std::find(weights_.begin(), weights_.end(), sorted_weights[0])  << std::endl;
    //    std::cout << "second highest weight index " << std::distance(weights_.begin(), (std::find(weights_.begin(), weights_.end(), sorted_weights[1]))) << std::endl;
    //    std::cout << "second highest weight " << *std::find(weights_.begin(), weights_.end(), sorted_weights[1]) << std::endl;

    //    //selecting the highest first 50 particles
    //    for(int i= 0; i < 50; ++i)
    //    {
    //        for(int j =0; j < (num_particles_/50); ++j)
    //        {
    //            resampled_state.push_back(filtered_pose[std::distance(weights_.begin(), (std::find(weights_.begin(), weights_.end(), sorted_weights[i])))]);
    //        }

    //    }

    //    for(int i =0; i < (num_particles_-(num_particles_/2)); ++i)
    //    {
    //        resampled_state.push_back(filtered_pose[std::distance(weights_.begin(), (std::max_element(weights_.begin(), weights_.end())-1))]);
    //    }



    if(!resampled_state.empty())
    {
        filtered_pose.clear(); filtered_pose.resize(resampled_state.size());
        filtered_pose = resampled_state;
    }

    Eigen::VectorXf avg_pose;
    avg_pose.resize(state_size_), avg_pose.setZero();
    for(int i = 0; i < num_particles_; ++i)
    {
        avg_pose(0) += filtered_pose[i](0);
        avg_pose(1) += filtered_pose[i](1);
        avg_pose(2) += filtered_pose[i](2);
        avg_pose(3) += filtered_pose[i](3);
        avg_pose(4) += filtered_pose[i](4);
        avg_pose(5) += filtered_pose[i](5);

    }

    avg_pose(0) = avg_pose(0)/num_particles_;
    avg_pose(1) = avg_pose(1)/num_particles_;
    avg_pose(2) = avg_pose(2)/num_particles_;
    avg_pose(3) = avg_pose(3)/num_particles_;
    avg_pose(4) = avg_pose(4)/num_particles_;
    avg_pose(5) = avg_pose(5)/num_particles_;


    //final_pose = avg_pose;
    std::cout << "avg_pose  " << avg_pose << std::endl;
    std::cout << "corresponding VO_pose " << VO_pose  << std::endl;


    return filtered_pose;

}


Eigen::VectorXf particle_filter::computeVectorInv(Eigen::VectorXf pose)
{
    Eigen::MatrixXf inv_pose_mat;
    inv_pose_mat = this->invTrans(pose(0), pose(1), pose(2), pose(3), pose(4), pose(5));

    Eigen::VectorXf inv_pose_vec;
    inv_pose_vec = this->compVecFromMat(inv_pose_mat);

    return inv_pose_vec;

}

Eigen::VectorXf particle_filter::computeDiffUsingHomoMethod(Eigen::VectorXf prev_pose, Eigen::VectorXf current_pose)
{
    Eigen::MatrixXf prev_pose_mat, current_pose_mat, pose_multiplied_mat;
    pose_multiplied_mat.resize(4,4);

    prev_pose_mat = this->compHomoTrans(prev_pose(0), prev_pose(1), prev_pose(2), prev_pose(3), prev_pose(4), prev_pose(5));
    current_pose_mat = this->compHomoTrans(current_pose(0),current_pose(1),current_pose(2),current_pose(3),current_pose(4),current_pose(5));

    pose_multiplied_mat = prev_pose_mat * current_pose_mat;

    Eigen::VectorXf pose_multiplied_vec;
    pose_multiplied_vec = compVecFromMat(pose_multiplied_mat);

    return pose_multiplied_vec;

}

Eigen::MatrixXf particle_filter::compHomoTrans(float ox, float oy, float oz, float r, float p, float y)
{
    Eigen::MatrixXf mat;
    mat.resize(4,4);

    mat(0,0)=cos(r)*cos(p); mat(0,1)=cos(r)*sin(p)*sin(y)-sin(r)*cos(y); mat(0,2)=cos(r)*sin(p)*cos(y)+sin(r)*sin(y); mat(0,3)=ox;
    mat(1,0)=sin(r)*cos(p); mat(1,1)=sin(r)*sin(p)*sin(y)+cos(r)*cos(y); mat(1,2)=sin(r)*sin(p)*cos(y)-cos(r)*sin(y); mat(1,3)=oy;
    mat(2,0)=-sin(p);		mat(2,1)=cos(p)*sin(y);					     mat(2,2)=cos(p)*cos(y);                      mat(2,3)=oz;
    mat(3,0)=0;             mat(3,1)=0;								     mat(3,2)=0;								  mat(3,3)=1;

    return mat;
}

Eigen::MatrixXf particle_filter::invTrans(float ox, float oy, float oz, float r, float p, float y)
{
    Eigen::MatrixXf mat;
    mat.resize(4,4);

    mat(0,0)=cos(r)*cos(p);					     mat(0,1)=sin(r)*cos(p);                      mat(0,2)=-sin(p);	    	mat(0,3)=-ox*cos(r)*cos(p)-oy*sin(r)*cos(p)+oz*sin(p);
    mat(1,0)=cos(r)*sin(p)*sin(y)-sin(r)*cos(y); mat(1,1)=sin(r)*sin(p)*sin(y)+cos(r)*cos(y); mat(1,2)=cos(p)*sin(y); 	mat(1,3)=ox*(sin(r)*cos(y)-cos(r)*sin(p)*sin(y))-oy*(cos(r)*cos(y)+sin(p)*sin(r)*sin(y))-oz*cos(p)*sin(y);
    mat(2,0)=cos(r)*sin(p)*cos(y)+sin(r)*sin(y); mat(2,1)=sin(r)*sin(p)*cos(y)-cos(r)*sin(y); mat(2,2)=cos(p)*cos(y);	mat(2,3)=-ox*(sin(r)*sin(y)+cos(r)*sin(p)*cos(y))+oy*(cos(r)*sin(y)-sin(p)*sin(r)*cos(y))-oz*cos(p)*cos(y);
    mat(3,0)=0;                                  mat(3,1)=0;								  mat(3,2)=0;				mat(3,3)=1;

    return mat;

}

Eigen::VectorXf particle_filter::compVecFromMat(Eigen::MatrixXf pose_mat)
{
    float x=pose_mat(0,3);
    float y=pose_mat(1,3);
    float z=pose_mat(2,3);

    float nx=pose_mat(0,0);      float ny=pose_mat(1,0); float nz=pose_mat(2,0);
    float ox=pose_mat(0,1);      float oy=pose_mat(1,1); float oz=pose_mat(2,1);
    float ax=pose_mat(0,2);      float ay=pose_mat(1,2); float az=pose_mat(2,2);

    float roll=atan2(ny,nx);
    float pitch=atan2(-nz,nx*cos(roll)+ny*sin(roll));
    float yaw=atan2(ax*sin(roll)-ay*cos(roll), -ox*sin(roll)+oy*cos(roll));


    Eigen::VectorXf pose_vec;
    pose_vec.resize(6), pose_vec.setZero();

    pose_vec(0) = x;
    pose_vec(1) = y;
    pose_vec(2) = z;

    pose_vec(3) = roll;
    pose_vec(4) = pitch;
    pose_vec(5) = yaw;

    return pose_vec;

}

void particle_filter::computeJacobian()
{

    //    state_vec_xk1k_(0) = state_vec_xkk_(0) + state_vec_xkk_(3)*deltaT + 0.5*state_vec_xkk_(6)*pow(deltaT,2); // x = x + vx*dt + ax*dt^2
    //    state_vec_xk1k_(1) = state_vec_xkk_(1) + state_vec_xkk_(4)*deltaT + 0.5*state_vec_xkk_(7)*pow(deltaT,2); // y = y + vy*dt + ay*dt^2
    //    state_vec_xk1k_(2) = state_vec_xkk_(2) + state_vec_xkk_(5)*deltaT + 0.5*state_vec_xkk_(8)*pow(deltaT,2); // z = z + vz*dt + az*dt^2
    //    state_vec_xk1k_(3) = state_vec_xkk_(3) + state_vec_xkk_(6)*deltaT;    // vx = vx + ax*dt
    //    state_vec_xk1k_(4) = state_vec_xkk_(4) + state_vec_xkk_(7)*deltaT;    // vy = vy + ay*dt
    //    state_vec_xk1k_(5) = state_vec_xkk_(5) + state_vec_xkk_(8)*deltaT;    // vz = vz + az*dt
    //    state_vec_xk1k_(6) = state_vec_xkk_(6); /*+ 0.01*/;   //ax = ax + n
    //    state_vec_xk1k_(7) = state_vec_xkk_(7); /*+ 0.01*/;   //ay = ay + n
    //    state_vec_xk1k_(8) = state_vec_xkk_(8); /*+ 0.01*/;   //az = az + n
    //    state_vec_xk1k_(9) = state_vec_xkk_(9) + state_vec_xkk_(12)*deltaT;   // thetax = thetax + wx*dt
    //    state_vec_xk1k_(10) = state_vec_xkk_(10) + state_vec_xkk_(13)*deltaT; // thetay = thetay + wy*dt
    //    state_vec_xk1k_(11) = state_vec_xkk_(11) + state_vec_xkk_(14)*deltaT; // thetaz = thetaz + wz*dt
    //    state_vec_xk1k_(12) = state_vec_xkk_(12) /*+ 0.01*/; // wx = wx + n
    //    state_vec_xk1k_(13) = state_vec_xkk_(13) /*+ 0.01*/; // wy = wy + n
    //    state_vec_xk1k_(14) = state_vec_xkk_(14) /*+ 0.01*/; // wz = wz + n

    jacobian_f_x_(0,0) = 1;
    jacobian_f_x_(1,1) = 1;
    jacobian_f_x_(2,2) = 1;
    jacobian_f_x_(3,3) = 1;
    jacobian_f_x_(4,4) = 1;
    jacobian_f_x_(5,5) = 1;

}

void particle_filter::computePredictionNoiseCov()
{

    VO_prediction_noise_cov_(0,0) = 0.03;
    VO_prediction_noise_cov_(1,1) = 0.03;
    VO_prediction_noise_cov_(2,2) = 0.03;
    VO_prediction_noise_cov_(3,3) = 0.03;
    VO_prediction_noise_cov_(4,4) = 0.03;
    VO_prediction_noise_cov_(5,5) = 0.03;

}

void particle_filter::transfromRobotToWorldFrame(float roll, float pitch, float yaw, Eigen::Matrix4f& rotation_mat)
{
    rotation_mat.setZero();

    rotation_mat(0,0) = cos(yaw)*cos(pitch);
    rotation_mat(0,1) = cos(yaw)*sin(pitch)*sin(roll) - sin(yaw)*cos(roll);
    rotation_mat(0,2) = cos(yaw)*sin(pitch)*cos(roll) + sin(yaw)*sin(pitch);

    rotation_mat(1,0) = sin(yaw)*cos(pitch);
    rotation_mat(1,1) = sin(yaw)*sin(pitch)*sin(roll) + cos(yaw)*cos(roll);
    rotation_mat(1,2) = sin(yaw)*sin(pitch)*cos(roll) - cos(yaw)*sin(roll);

    rotation_mat(2,0) = -sin(pitch);
    rotation_mat(2,1) = cos(pitch)*sin(roll);
    rotation_mat(2,2) = cos(pitch)*cos(roll);
    rotation_mat(3,3) = 1;

}

