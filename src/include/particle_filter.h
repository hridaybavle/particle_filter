#include <iostream>
#include <string>
#include <math.h>
#include <fstream>

#include "ros/ros.h"
#include "eigen3/Eigen/Eigen"
#include <binders.h>

const float maha_dist_threshold = 100;

class particle_filter
{

public:
    particle_filter();
    ~particle_filter();

    struct particles {
        int id;
        double x;
        double y;
        double z;
        double roll;
        double pitch;
        double yaw;
        double weight;

    };

    struct obs_vector
    {
        int obs_id;
        Eigen::Vector3f map_pose;
        double maha_dist;
    };

    struct object_info_struct_pf {
        int id;
        std::string type;
        float prob;
        float num_points;
        Eigen::Vector3f pose;

    };

private:
    int iteration_num_;
    std::ofstream weights_dist_data_;

    int state_size_, num_particles_;
    std::vector<particles> particles_vec_;
    Eigen::VectorXf vo_prev_pose_, vo_current_pose_;
    Eigen::MatrixXf jacobian_f_x_, VO_prediction_cov_, VO_prediction_noise_cov_;

    std::vector<float> weights_;

    std::vector<obs_vector> arucos_map_;
    std::vector<obs_vector> arucos_map_filtered_;

    std::vector<object_info_struct_pf> object_map_;


    void transfromRobotToWorldFrame(float roll, float pitch, float yaw, Eigen::Matrix4f &rotation_mat);

public:
    void predictionIMU(Eigen::Vector4f imu_world_acc, Eigen::Vector4f imu_world_ang_vel);
    std::vector<Eigen::VectorXf> predictionVO(float deltaT, Eigen::VectorXf vo_pose_world, std::vector<Eigen::VectorXf> filtered_pose, Eigen::VectorXf &final_pose);
    void computeJacobian();
    void computePredictionNoiseCov();

    Eigen::VectorXf computeVectorInv(Eigen::VectorXf pose);
    Eigen::MatrixXf invTrans(float ox, float oy, float oz, float r, float p, float y);

    Eigen::VectorXf compVecFromMat(Eigen::MatrixXf pose_mat);

    Eigen::MatrixXf compHomoTrans(float ox, float oy, float oz, float r, float p, float y);
    Eigen::VectorXf computeDiffUsingHomoMethod(Eigen::VectorXf prev_pose, Eigen::VectorXf current_pose);
    std::vector<Eigen::VectorXf> init(int state_size, int num_particles, std::vector<Eigen::Vector3f> aruco_poses, std::vector<object_info_struct_pf> mapped_objects);


    std::vector<Eigen::VectorXf> bebopIMUUpdate(float roll, float pitch, float yaw, std::vector<Eigen::VectorXf> filtered_pose, Eigen::VectorXf &final_pose);
    std::vector<Eigen::VectorXf> arucoMapAndUpdate(std::vector<Eigen::Vector4f> aruco_pose, std::vector<Eigen::VectorXf> filtered_pose, Eigen::VectorXf &final_pose, Eigen::VectorXf VO_pose);
    std::vector<Eigen::VectorXf> slamdunkPoseUpdate(Eigen::Vector3f slamdunk_pose, std::vector<Eigen::VectorXf> filtered_pose, Eigen::VectorXf VO_pose);
    std::vector<Eigen::VectorXf> ObjectMapAndUpdate(object_info_struct_pf complete_object_pose, std::vector<Eigen::VectorXf> filtered_pose, Eigen::VectorXf &final_pose, Eigen::VectorXf VO_pose);

};
